package com.rest.api.constant;


import org.springframework.stereotype.Component;

@Component
public class ApplicationConstants {
    public static final String RESOURCE_URL_FININ_PICTURE = "/public/finin/photo/";
}
