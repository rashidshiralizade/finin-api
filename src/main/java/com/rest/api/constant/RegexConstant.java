package com.rest.api.constant;

public class RegexConstant {

    public static final String EMAIL = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";

    public static final String FIN = "^[A-z | 0-9]{7}$";
    public static final String SERIALID = "^[0-9]{8}$";
}
