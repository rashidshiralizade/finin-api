package com.rest.api.cyrpto;

import com.rest.api.cyrpto.PWDTK_DOTNET451.PWDTK;

public class UserPasswordEncoder {
    private String hash;
    private String salt;

    public static UserPasswordEncoder create(String password) {
        UserPasswordEncoder pass = new UserPasswordEncoder();
        byte[] randomSalt = PWDTK.getRandomSalt();

        pass.salt = PWDTK.hashBytesToHexString(randomSalt);
        pass.hash = PWDTK.hashBytesToHexString(PWDTK.passwordToHash(randomSalt, password, 10000));

        return pass;
    }

    public static UserPasswordEncoder create(String hash, String salt) {
        UserPasswordEncoder pass = new UserPasswordEncoder();

        pass.salt = salt;
        pass.hash = hash;

        return pass;
    }

    public String getHash() {
        return hash;
    }

    public String getSalt() {
        return salt;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((hash == null) ? 0 : hash.hashCode());
        result = prime * result + ((salt == null) ? 0 : salt.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object arg0) {
        // TODO Auto-generated method stub
        return super.equals(arg0);
    }

    public boolean compare(String password) {
        if (password == null)
            return false;

        byte[] saltbyte = PWDTK.hashHexStringToBytes(this.salt);
        byte[] hashPass = PWDTK.hashHexStringToBytes(this.hash);

        return PWDTK.comparePasswordToHash(saltbyte, password, hashPass, 10000);
    }
}
