package com.rest.api.cyrpto;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 * 
 * Converted from C# class
 * 
 * @author  Rashid Shiralizade
 * 
 */
public class Rfc2898 {

	// I made the variable names match the definition in RFC2898 - PBKDF2 where
	// possible, so you can trace the code functionality back to the
	// specification
	public static boolean IsLittleEndian = false;

	private static Mac _hmacsha512Obj;
	private static int hLen;
	private static byte[] P;
	private static byte[] S;
	private static int c;
	private static int dkLen;

	// Minimum rcommended itereations in Rfc2898
	public static final int CMinIterations = 1000;
	// Minimum recommended salt length in Rfc2898
	public static final int CMinSaltLength = 8;

	// / <summary>
	// / Rfc2898 constructor to create Rfc2898 object ready to perform Rfc2898
	// / functionality
	// / </summary>
	// / <param name="password">The Password to be hashed and is also the HMAC
	// / key</param>
	// / <param name="salt">Salt to be concatenated with the password</param>
	// / <param name="iterations">Number of iterations to perform HMACSHA
	// Hashing
	// / for PBKDF2</param>
	public Rfc2898(byte[] password, byte[] salt, int iterations) {
		hLen = 512 / 8;
		P = password;
		S = salt;
		c = iterations;

		try {
			_hmacsha512Obj = Mac.getInstance("HmacSHA512");
			SecretKeySpec secretkey = new SecretKeySpec(password, "HmacSHA512");
			_hmacsha512Obj.init(secretkey);
		} catch (Exception e) {
		}
	}

	// / <summary>
	// / Rfc2898 constructor to create Rfc2898 object ready to perform Rfc2898
	// / functionality
	// / </summary>
	// / <param name="password">The Password to be hashed and is also the HMAC
	// / key</param>
	// / <param name="salt">Salt to be concatenated with the password</param>
	// / <param name="iterations">Number of iterations to perform HMACSHA
	// Hashing
	// / for PBKDF2</param>
	public Rfc2898(String password, byte[] salt, int iterations) throws UnsupportedEncodingException {
		this(password.getBytes("UTF-8"), salt, iterations);
	}

	// / <summary>
	// / Rfc2898 constructor to create Rfc2898 object ready to perform Rfc2898
	// / functionality
	// / </summary>
	// / <param name="password">The Password to be hashed and is also the HMAC
	// / key</param>
	// / <param name="salt">Salt to be concatenated with the password</param>
	// / <param name="iterations">Number of iterations to perform HMACSHA
	// Hashing
	// / for PBKDF2</param>
	public Rfc2898(String password, String salt, int iterations) throws UnsupportedEncodingException {
		this(password.getBytes("UTF-8"), salt.getBytes("UTF-8"), iterations);
	}

	// / <summary>
	// / Derive Key Bytes using PBKDF2 specification listed in Rfc2898 and
	// / HMACSHA512 as the underlying PRF (Psuedo Random Function)
	// / </summary>
	// / <param name="keyLength">Length in Bytes of Derived Key</param>
	// / <returns>Derived Key</returns>
	public byte[] getDerivedKeyBytesPBKDF2HMACSHA512(int keyLength) {
		// no need to throw exception for dkLen too long as per spec because
		// dkLen cannot be larger than int.MaxValue so not worth the overhead to
		// check
		dkLen = keyLength;

		Double l = Math.ceil((double) dkLen / hLen);

		byte[] finalBlock = new byte[0];

		for (int i = 1; i <= l; i++) {
			// Concatenate each block from F into the final block (T_1..T_l)
			finalBlock = pMergeByteArrays(finalBlock, f(P, S, c, i));
		}

		// returning DK note r not used as dkLen bytes of the final concatenated
		// block returned rather than <0...r-1> substring of final intermediate
		// block + prior blocks as per spec
		// baxmaq *******************************
		return Arrays.copyOf(finalBlock, dkLen);

	}

	// / <summary>
	// / A static publicly exposed version of
	// / GetDerivedKeyBytes_PBKDF2_HMACSHA512 which matches the exact
	// / specification in Rfc2898 PBKDF2 using HMACSHA512
	// / </summary>
	// / <param name="P">Password passed as a Byte Array</param>
	// / <param name="S">Salt passed as a Byte Array</param>
	// / <param name="c">Iterations to perform the underlying PRF over</param>
	// / <param name="dkLen">Length of Bytes to return, an AES 256 key wold
	// / require 32 Bytes</param>
	// / <returns>Derived Key in Byte Array form ready for use by chosen
	// / encryption function</returns>
	public static byte[] pbkdf2(byte[] P, byte[] S, int c, int dkLen) {
		Rfc2898 rfcObj = new Rfc2898(P, S, c);
		return rfcObj.getDerivedKeyBytesPBKDF2HMACSHA512(dkLen);
	}

	// Main Function F as defined in Rfc2898 PBKDF2 spec
	private byte[] f(byte[] P, byte[] S, int c, int i) {

		// Salt and Block number Int(i) concatenated as per spec
		byte[] Si = pMergeByteArrays(S, intToEndian(i));

		// Initial hash (U_1) using password and salt concatenated with Int(i)
		// as per spec
		byte[] temp = prf(P, Si);

		// Output block filled with initial hash value or U_1 as per spec
		byte[] U_c = temp;

		for (int C = 1; C < c; C++) {
			// rehashing the password using the previous hash value as salt as
			// per spec
			temp = prf(P, temp);

			for (int j = 0; j < temp.length; j++) {
				// xor each byte of the each hash block with each byte of the
				// output block as per spec
				U_c[j] = (byte) (U_c[j] ^ temp[j]);
			}
		}

		// return a T_i block for concatenation to create the final block as per
		// spec
		return U_c;
	}

	// PRF function as defined in Rfc2898 PBKDF2 spec
	private byte[] prf(byte[] P, byte[] S) {
		// HMACSHA512 Hashing, better than the HMACSHA1 in Microsofts
		// implementation ;)
		return _hmacsha512Obj.doFinal(pMergeByteArrays(P, S));
	}

	private byte[] encodeHmacsha512Obj(String serializedModel, byte[] key) {
		try {
			SecretKeySpec secretkey = new SecretKeySpec(P, "HmacSHA512");

			_hmacsha512Obj.init(secretkey);

			return _hmacsha512Obj.doFinal(serializedModel.getBytes("UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	// This method returns the 4 octet encoded int with most significant bit
	// first as per spec
	private byte[] intToEndian(int i) {
		ByteBuffer bb = ByteBuffer.allocate(4);

		if (IsLittleEndian) {
			bb.order(ByteOrder.LITTLE_ENDIAN);
		} else {
			bb.order(ByteOrder.BIG_ENDIAN);
		}

		bb.putInt(i);
		bb.flip();

		/*
		 * // Make sure most significant bit is first if (BitConverter.IsLittleEndian) {
		 * Array.Reverse(I); }
		 */

		return bb.array();
	}

	// Merge two arrays into a new array
	private byte[] pMergeByteArrays(byte[] source1, byte[] source2) {
		// Most efficient way to merge two arrays this according to
		// http://stackoverflow.com/questions/415291/best-way-to-combine-two-or-more-byte-arrays-in-c-sharp
		byte[] buffer = new byte[source1.length + source2.length];

		System.arraycopy(source1, 0, buffer, 0, source1.length);
		System.arraycopy(source2, 0, buffer, source1.length, source2.length);

		return buffer;
	}

}