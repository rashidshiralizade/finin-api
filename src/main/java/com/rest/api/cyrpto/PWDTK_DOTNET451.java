package com.rest.api.cyrpto;

import java.io.UnsupportedEncodingException;
import java.util.Random;

/**
 * 
 * Converted from C# class
 * 
 * @author  Rashid Shiralizade
 * 
 */
public class PWDTK_DOTNET451 {
	// / <summary>
	// / Password Toolkit created by Thashiznets
	// / This library facilitates crypto random salt generating and password
	// / hashing using HMACSHA512 based PBKDF2
	// / Also it provides hash comparisson and password policy enforcement by
	// / regex (optional)
	// / Created by thashiznets@yahoo.com.au
	// / v2.0.0.2
	// / </summary>
	public static class PWDTK {
		// / <summary>
		// / The default character length to create salt strings
		// / </summary>
		public final static int CDefaultSaltLength = 64;

		// / <summary>
		// / The default iteration count for key stretching
		// / </summary>
		public static int CDefaultIterationCount = 1000;

		// / <summary>
		// / The minimum size in characters the password hashing function will
		// / allow for a salt string, salt must be always greater than 8 for
		// / PBKDF2 key derivitation to function
		// / </summary>
		public final static int CMinSaltLength = 8;

		// / <summary>
		// / The maximum size a password can be to avoid massive passwords that
		// / force the initial hash to take so long it creates a DOS effect. Be
		// / careful increasing this, also make sure you catch exception for
		// / password too long!
		// / </summary>
		public final static int CMaxPasswordLength = 1024;

		// / <summary>
		// / The key length used in the PBKDF2 derive bytes and matches the
		// / output of the underlying HMACSHA512 psuedo random function
		// / </summary>
		public final static int CKeyLength = 64;

		// / <summary>
		// / A default password policy provided for use if you are unsure what
		// to
		// / make your own PasswordPolicy
		// / </summary>
		public static PasswordPolicy CDefaultPasswordPolicy = new PasswordPolicy(1, 1, 2, 6, Integer.MAX_VALUE);

		// / <summary>
		// / Below are regular expressions used for password to policy
		// / comparrisons
		// / </summary>
		private final static String CNumbersRegex = "[\\d]";
		private final static String CUppercaseRegex = "[A-Z]";
		private final static String CNonAlphaNumericRegex = "[^0-9a-zA-Z]";

		// Used for hex convert.
		private final static char[] hexArray = "0123456789ABCDEF".toCharArray();

		// / <summary>
		// / A PasswordPolicy defines min and max password length and also
		// / minimum amount of Uppercase, Non-Alpanum and Numerics to be present
		// / in the password string
		// / </summary>
		public static class PasswordPolicy {
			private int _aForceXUpperCase;
			private int _aForceXNonAlphaNumeric;
			private int _aForceXNumeric;
			private int _aPasswordMinLength;
			private int _aPasswordMaxLength;

			// / <summary>
			// / Creates a new PasswordPolicy Struct
			// / </summary>
			// / <param name="xUpper">Forces at least this number of Uppercase
			// / characters</param>
			// / <param name="xNonAlphaNumeric">Forces at least this number of
			// / Special characters</param>
			// / <param name="xNumeric">Forces at least this number of Numeric
			// / characters</param>
			// / <param name="minLength">Forces at least this number of
			// / characters</param>
			// / <param name="maxLength">Forces at most this number of
			// / characters</param>
			public PasswordPolicy(int xUpper, int xNonAlphaNumeric, int xNumeric, int minLength, int maxLength) {
				_aForceXUpperCase = xUpper;
				_aForceXNonAlphaNumeric = xNonAlphaNumeric;
				_aForceXNumeric = xNumeric;
				_aPasswordMinLength = minLength;
				_aPasswordMaxLength = maxLength;
			}

		}

		/*
		 * /// <summary> /// Convert a String into a SecureString and attempt to remove
		 * the original String from RAM /// </summary> /// <param name="toConvert">This
		 * is the String to convert into a SecureString, please note that String
		 * ToConvert will be Overwritten with the same length of *'s to try remove it
		 * from RAM so after conversion it can be used no more and the SecureString must
		 * be used instead</param> /// <returns>A SecureString which resides in memory
		 * in an encrypted state</returns> public static SecureString
		 * StringToSecureString(String toConvert) { SecureString outputSecureString =
		 * new SecureString(); String overwriteString = String.Empty;
		 * 
		 * foreach (Char c in toConvert) { outputSecureString.AppendChar(c); }
		 * 
		 * //Overwrite original String to try clear it from RAM (Note may still reside
		 * in Paged and Hiberfil) for (int i = 0; i < toConvert.Length; i++) {
		 * overwriteString += "*"; }
		 * 
		 * toConvert = overwriteString;
		 * 
		 * return outputSecureString; }
		 */

		// / <summary>
		// / Crypto Randomly generates a byte array that can be used safely as
		// / salt
		// / </summary>
		// / <param name="saltLength">Length of salt Byte Array to be generated.
		// / Defaults to 64</param>
		// / <returns>A Byte Array to be used as Salt</returns>
		public static byte[] getRandomSalt() {
			return getRandomSalt(CDefaultSaltLength);
		}

		public static byte[] getRandomSalt(int saltLength) {
			return pGenerateRandomSalt(saltLength);
		}

		// / <summary>
		// / Crypto Randomly generates a byte array that can be used safely as
		// / salt and returns it as a HEX string.
		// / </summary>
		// / <param name="saltLength">Length of salt HEX string to be generated.
		// / Defaults to 64</param>
		// / <returns></returns>
		public static String getRandomSaltHexString() {
			return getRandomSaltHexString(CDefaultSaltLength);
		}

		public static String getRandomSaltHexString(int saltLength) {
			return hashBytesToHexString(getRandomSalt(saltLength));
		}

		// / <summary>
		// / Input a password and a hash value in bytes and it uses PBKDF2
		// / HMACSHA512 to hash the password and compare it to the supplied hash
		// / </summary>
		// / <param name="password">The text password to be hashed for
		// / comparrison</param>
		// / <param name="salt">The salt to be added to the password pre
		// / hash</param>
		// / <param name="hash">The existing hash byte array you have stored for
		// / comparison</param>
		// / <param name="iterationCount">The number of times you have specified
		// / to hash the password for key stretching. Default is 5000
		// / iterations</param>
		// / <returns>True if Password matches Hash else returns false</returns>
		public static Boolean comparePasswordToHash(byte[] salt, String password, byte[] hash) {
			return comparePasswordToHash(salt, password, hash, CDefaultIterationCount);
		}

		public static Boolean comparePasswordToHash(byte[] salt, String password, byte[] hash, int iterationCount) {

			try {
				return sequenceEqual(pPasswordToHash(salt, password.getBytes("UTF-8"), iterationCount), hash);
			} catch (Exception e) {
			}

			return false;
		}

		private static boolean sequenceEqual(byte[] arg1, byte[] arg2) {
			if (arg1.length != arg2.length) {
				return false;
			}

			for (int i = 0; i < arg2.length; i++) {
				if (arg1[i] != arg2[i]) {
					return false;
				}
			}

			return true;
		}

		// / <summary>
		// / Converts Salt + Password into a Hash
		// / </summary>
		// / <param name="salt">The salt to add infront of the password before
		// / processing the hash (Anti-Rainbow Table tactic)</param>
		// / <param name="password">The password used to compute the
		// hash</param>
		// / <param name="iterationCount">Repeat the PBKDF2 dunction this many
		// / times (Anti-Rainbow Table tactic), higher value = more CPU usage
		// / which is better defence against cracking. Default is 5000
		// / iterations</param>
		// / <returns>The Hash value of the salt + password as a Byte
		// / Array</returns>
		public static byte[] passwordToHash(byte[] salt, String password) {
			return passwordToHash(salt, password, CDefaultIterationCount);
		}

		public static byte[] passwordToHash(byte[] salt, String password, int iterationCount) {
			pCheckSaltCompliance(salt);

			byte[] convertedPassword = stringToUtf8Bytes(password);

			pCheckPasswordSizeCompliance(convertedPassword);

			return pPasswordToHash(salt, convertedPassword, iterationCount);
		}

		// / <summary>
		// / Converts Salt + Password into a Hash represented as a HEX String
		// / </summary>
		// / <param name="salt">The salt to add infront of the password before
		// / processing the hash (Anti-Rainbow Table tactic)</param>
		// / <param name="password">The password used to compute the
		// hash</param>
		// / <param name="iterationCount">Repeat the PBKDF2 dunction this many
		// / times (Anti-Rainbow Table tactic), higher value = more CPU usage
		// / which is better defence against cracking. Default is 5000
		// / iterations</param>
		// / <returns>The Hash value of the salt + password as a HEX
		// / String</returns>

		public static String passwordToHashHexString(byte[] salt, String password) {
			return passwordToHashHexString(salt, password, CDefaultIterationCount);
		}

		public static String passwordToHashHexString(byte[] salt, String password, int iterationCount) {
			pCheckSaltCompliance(salt);

			byte[] convertedPassword = stringToUtf8Bytes(password);

			pCheckPasswordSizeCompliance(convertedPassword);

			return hashBytesToHexString(pPasswordToHash(salt, convertedPassword, iterationCount));
		}

		// / <summary>
		// / Converts the Byte array Hash into a Human Friendly HEX String
		// / </summary>
		// / <param name="hash">The Hash value to convert</param>
		// / <returns>A HEX String representation of the Hash value</returns>
		public static String hashBytesToHexString(byte[] hash) {
			char[] hexChars = new char[hash.length * 2];
			for (int j = 0; j < hash.length; j++) {
				int v = hash[j] & 0xFF;
				hexChars[j * 2] = hexArray[v >>> 4];
				hexChars[j * 2 + 1] = hexArray[v & 0x0F];
			}
			return new String(hexChars);
		}

		// / <summary>
		// / Converts the Hash Hex String into a Byte[] for computational
		// / processing
		// / </summary>
		// / <param name="hashHexString">The Hash Hex String to convert back to
		// / bytes</param>
		// / <returns>Esentially reverses the HashToHexString function, turns
		// the
		// / String back into Bytes</returns>
		public static byte[] hashHexStringToBytes(String hashHexString) {
			int len = hashHexString.length();
			byte[] data = new byte[len / 2];
			for (int i = 0; i < len; i += 2) {
				data[i / 2] = (byte) ((Character.digit(hashHexString.charAt(i), 16) << 4)
						+ Character.digit(hashHexString.charAt(i + 1), 16));
			}
			return data;
		}

		// / <summary>
		// / Tests the password for compliance against the supplied password
		// / policy
		// / </summary>
		// / <param name="password">The password to test for compliance</param>
		// / <param name="pwdPolicy">The PasswordPolicy that we are testing that
		// / the Password complies with</param>
		// / <returns>True for Password Compliance with the Policy</returns>
		public static Boolean tryPasswordPolicyCompliance(String password, PasswordPolicy pwdPolicy) {
			try {
				pCheckPasswordPolicyCompliance(password, pwdPolicy);
				return true;
			} catch (Exception e) {

			}

			return false;
		}

		// / <summary>
		// / Tests the password for compliance against the supplied password
		// / policy
		// / </summary>
		// / <param name="password">The password to test for compliance</param>
		// / <param name="pwdPolicy">The PasswordPolicy that we are testing that
		// / the Password complies with</param>
		// / <param name="pwdPolicyException">The exception that will contain
		// why
		// / the Password does not meet the PasswordPolicy</param>
		// / <returns>True for Password Compliance with the Policy</returns>
		public static Boolean tryPasswordPolicyCompliance(String password, PasswordPolicy pwdPolicy,
				PasswordPolicyException pwdPolicyException) {
			try {
				pCheckPasswordPolicyCompliance(password, pwdPolicy);
				return true;
			} catch (PasswordPolicyException ex) {
				pwdPolicyException = ex;
			}

			return false;
		}

		// / <summary>
		// / Converts String to UTF8 friendly Byte Array
		// / </summary>
		// / <param name="stringToConvert">String to convert to Byte
		// / Array</param>
		// / <returns>A UTF8 decoded string as Byte Array</returns>
		public static byte[] stringToUtf8Bytes(String stringToConvert) {

			try {
				return stringToConvert.getBytes("UTF-8");
			} catch (Exception e) {
			}

			return null;
		}

		// / <summary>
		// / Converts UTF8 friendly Byte Array to String
		// / </summary>
		// / <param name="bytesToConvert">Byte Array to convert to
		// String</param>
		// / <returns>A UTF8 encoded Byte Array as String</returns>
		public static String utf8BytesToString(byte[] bytesToConvert) {
			try {
				return new String(bytesToConvert, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		private static void pCheckPasswordPolicyCompliance(String password, PasswordPolicy pwdPolicy) {
			if (new Regex(CNumbersRegex).matchesCount(password) < pwdPolicy._aForceXNumeric) {
				throw new PasswordPolicyException(
						"The password must contain " + pwdPolicy._aForceXNumeric + " numeric [0-9] characters");
			}

			if (new Regex(CNonAlphaNumericRegex).matchesCount(password) < pwdPolicy._aForceXNonAlphaNumeric) {
				throw new PasswordPolicyException(
						"The password must contain " + pwdPolicy._aForceXNonAlphaNumeric + " special characters");
			}

			if (new Regex(CUppercaseRegex).matchesCount(password) < pwdPolicy._aForceXUpperCase) {
				throw new PasswordPolicyException(
						"The password must contain " + pwdPolicy._aForceXUpperCase + " uppercase characters");
			}

			if (password.length() < pwdPolicy._aPasswordMinLength) {
				throw new PasswordPolicyException("The password does not have a length of at least "
						+ pwdPolicy._aPasswordMinLength + " characters");
			}

			if (password.length() > pwdPolicy._aPasswordMaxLength) {
				throw new PasswordPolicyException(
						"The password is longer than " + pwdPolicy._aPasswordMaxLength + " characters");
			}
		}

		private static byte[] pGenerateRandomSalt(int saltLength) {
			byte[] salt = new byte[saltLength];

			Random rn = new Random();

			for (int i = 0; i < salt.length; i++) {
				salt[i] = (byte) rn.nextInt();
			}

			return salt;
		}

		private static void pCheckSaltCompliance(byte[] salt) {
			if (salt.length < CMinSaltLength) {
				throw new SaltTooShortException("The supplied salt is too short, it must be at least " + CMinSaltLength
						+ " bytes long as defined by CMinSaltLength");
			}
		}

		private static void pCheckPasswordSizeCompliance(byte[] password) {
			if (password.length > CMaxPasswordLength) {
				throw new PasswordTooLongException(
						"The supplied password is longer than allowed, it must be smaller than " + CMaxPasswordLength
								+ " bytes long as defined by CMaxPasswordLength");
			}
		}

		private static byte[] pPasswordToHash(byte[] salt, byte[] password, int iterationCount) {
			return new Rfc2898(password, salt, iterationCount).getDerivedKeyBytesPBKDF2HMACSHA512(CKeyLength);
		}

	}

	public static class SaltTooShortException extends RuntimeException {
		public SaltTooShortException(String message) {
			super(message);
		}
	}

	public static class PasswordPolicyException extends RuntimeException {
		public PasswordPolicyException(String message) {
			super(message);
		}
	}

	public static class PasswordTooLongException extends RuntimeException {
		public PasswordTooLongException(String message) {
			super(message);
		}
	}

}