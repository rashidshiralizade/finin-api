package com.rest.api.cyrpto;

/**
 * 
 * Created for simplifying converting of C# classes
 * 
 * @author  Rashid Shiralizade
 * 
 */
public class Regex {
	private String regex;

	public Regex(String regex) {
		this.regex = regex;
	}

	public Integer matchesCount(String arg) {
		return arg.split(regex).length;
	}
}
