package com.rest.api.config;

import com.rest.api.filter.CorsAccessFilter;
import com.rest.api.security.DefaultAuthenticationEntryPoint;
import com.rest.api.security.DefaultAuthenticationFilter;
import com.rest.api.security.DefaultAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import javax.servlet.Filter;

@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DefaultAuthenticationProvider defaultAuthenticationProvider;

    @Autowired
    private DefaultAuthenticationEntryPoint defaultAuthenticationEntryPoint;

    /**
     *
     * @param http gelen requestler "public" linkinden gelirse icaze var
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()//
                .addFilterBefore(corsFilter(), BasicAuthenticationFilter.class) //
                .addFilterBefore(authenticationFilter(), BasicAuthenticationFilter.class) //
                .authenticationProvider(defaultAuthenticationProvider) //
                .authorizeRequests() //
                .antMatchers("/public/**").permitAll() //
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()//
                .anyRequest()//
                .authenticated()//
                .and()//
                .exceptionHandling().accessDeniedPage("/public/access/denied")//
                .authenticationEntryPoint(defaultAuthenticationEntryPoint);

        http.headers()//
                .frameOptions().disable()//
                .cacheControl().disable() //
                .httpStrictTransportSecurity().disable();
    }

    //Swaggere icaze veririk
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/v2/api-docs/**", "/configuration/ui/**", "/swagger-resources/**",
                "/configuration/security/**", "/swagger-ui.html", "/webjars/**");
        web.ignoring().antMatchers(HttpMethod.OPTIONS);
    }

    @Bean
    public Filter corsFilter() {
        return new CorsAccessFilter();
    }

    @Bean
    public Filter authenticationFilter() {
        return new DefaultAuthenticationFilter();
    }


}
