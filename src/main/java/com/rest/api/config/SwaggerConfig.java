package com.rest.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    private String host = "localhost:8085";


    @Bean
    public Docket productUnregApi1() {
        return new Docket(DocumentationType.SWAGGER_2) //
                .groupName("FinIn services") //
                .globalOperationParameters(globalParametersForAll()) //
                .host(host) //
                //     .ignoredParameterTypes(User.class) //
                .select() //
                .apis(RequestHandlerSelectors.basePackage("com.rest.api.controller")) //
                .build() //
                .apiInfo(metadata());
    }


    private List<Parameter> globalParametersForAll() {
        List<Parameter> parameter = new ArrayList<Parameter>();

        parameter.add(new ParameterBuilder()//
                .name("Accept-Language")//
                .description("Language header") //
                .modelRef(new ModelRef("string")) //
                .parameterType("header")//
                .required(false)//
                .build());

        return parameter;
    }

    private List<Parameter> globalParametersForRegistered() {
        List<Parameter> parameter = new ArrayList<Parameter>();

        parameter.add(new ParameterBuilder()//
                .name("Authorization")//
                .description("Authorization header") //
                .modelRef(new ModelRef("string")) //
                .parameterType("header")//
                .required(true)//
                .build());

        return parameter;
    }

    private ApiInfo metadata() {
        return new ApiInfoBuilder() //
                .title("FININN  documentation")//
                .description("FININN documentation")//
                .version("1.0.0")//
                .build();
    }
}

