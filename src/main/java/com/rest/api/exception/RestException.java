package com.rest.api.exception;

import com.rest.api.enums.ErrorEnum;

public class RestException extends RuntimeException {

    private int code;
    private String message;

    public RestException(ErrorEnum errorEnum) {
        this.code = errorEnum.getValue();
        this.message = errorEnum.name().toLowerCase();
    }

    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
