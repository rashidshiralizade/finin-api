package com.rest.api.response;

public class StatusResponse {

    private int code;
    private String message;

    public StatusResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
