package com.rest.api.response;

import com.rest.api.model.DepositTransaction;

import java.util.Date;
import java.util.List;

public class DepositResponse {

    private Integer id;
    private DepositPeriodResponse depositPeriodResponse;
    private int idUser;
    private Integer amount;
    private Date insertDate;
    private List<DepositTransactionResponse> depositTransactionResponse;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DepositPeriodResponse getDepositPeriodResponse() {
        return depositPeriodResponse;
    }

    public void setDepositPeriodResponse(DepositPeriodResponse depositPeriodResponse) {
        this.depositPeriodResponse = depositPeriodResponse;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public List<DepositTransactionResponse> getDepositTransactionResponse() {
        return depositTransactionResponse;
    }

    public void setDepositTransactionResponse(List<DepositTransactionResponse> depositTransactionResponse) {
        this.depositTransactionResponse = depositTransactionResponse;
    }
}
