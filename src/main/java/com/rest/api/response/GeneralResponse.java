package com.rest.api.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.rest.api.enums.ErrorEnum;

public class GeneralResponse {

    private StatusResponse status;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object data;


    public GeneralResponse(int code, String message) {
        this.status = new StatusResponse(code, message);
    }

    public GeneralResponse(ErrorEnum errorEnum, Object data) {
        this.status = new StatusResponse(errorEnum.getValue(), errorEnum.name().toLowerCase());
        this.data = data;
    }

    public GeneralResponse(ErrorEnum errorEnum) {
        this.status = new StatusResponse(errorEnum.getValue(), errorEnum.name().toLowerCase());
    }


    public StatusResponse getStatus() {
        return status;
    }

    public Object getData() {
        return data;
    }
}
