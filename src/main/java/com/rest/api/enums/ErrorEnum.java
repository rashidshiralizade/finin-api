package com.rest.api.enums;

public enum ErrorEnum {

    OK(200),//
    VALIDATION_ERROR(203),//
    INVALID_TOKEN(204),//
    CLIENT_NOT_AUTHORIZED(205),//
    USERNAME_OR_PASSWORD_WRONG(207),//
    USERNAME_ALREADY_EXISTS(209),//
    TOKEN_NOT_FOUND(210),//
    USER_BLOCKED(212),//
    USER_PENDING(213),//
    ACCESS_DENIED(403),//
    INVALID_URL(404),//
    SERVER_ERROR(500),//
    DEPOSIT_NOT_FOUND(214),//
    DENIED_ADD_DEPOSIT(218),//
    DEPOSIT_PERIOD_NOT_FOUND(215);//



    private int value;

    private ErrorEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
