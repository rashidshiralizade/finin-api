package com.rest.api.enums;

import com.rest.api.exception.RestException;

public enum DataStatusEnum {

    DELETED(0), ACTIVE(1), UPDATED(2);

    private Short value;

    private DataStatusEnum(int value) {
        this.value = (short) value;
    }

    public short getValue() {
        return value;
    }

    public static DataStatusEnum getDataStatus(Short value) {
        for (DataStatusEnum dataStatusEnum : values()) {
            if (dataStatusEnum.value.equals(value)) {
                return dataStatusEnum;
            }
        }
        throw new RestException(ErrorEnum.SERVER_ERROR);
    }
}
