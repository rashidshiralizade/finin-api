package com.rest.api.enums;

import com.rest.api.exception.RestException;

public enum UserStatusEnum {

    PENDING(1),//
    COMPLETED(2),//
    BLOCKED(3);

    private Short value;

    private UserStatusEnum(int value) {
        this.value = (short) value;
    }

    public short getValue() {
        return value;
    }

    public static UserStatusEnum getUserStatus(Short value) {
        for (UserStatusEnum userStatusEnum : values()) {
            if (userStatusEnum.value.equals(value)) {
                return userStatusEnum;
            }
        }
        throw new RestException(ErrorEnum.SERVER_ERROR);
    }
}

