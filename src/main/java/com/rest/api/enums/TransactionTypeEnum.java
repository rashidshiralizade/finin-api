package com.rest.api.enums;

import com.rest.api.exception.RestException;

public enum TransactionTypeEnum {

    ADD(1), SUB(2);

    private Short value;

    private TransactionTypeEnum(int value) {
        this.value = (short) value;
    }

    public short getValue() {
        return value;
    }

    public static TransactionTypeEnum getTransactionType(Short value) {
        for (TransactionTypeEnum transactionTypeEnum : values()) {
            if (transactionTypeEnum.value.equals(value)) {
                return transactionTypeEnum;
            }
        }
        throw new RestException(ErrorEnum.SERVER_ERROR);
    }
}
