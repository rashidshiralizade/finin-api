package com.rest.api.repository;

import com.rest.api.model.Deposit;
import com.rest.api.model.DepositTransaction;
import com.rest.api.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DepositTransactionRepository extends DefaultCrudRepository<DepositTransaction> {

    List<DepositTransaction> findAllByDeposit(Deposit deposit);

    @Query("select dt from DepositTransaction dt where dt.deposit.user=:user")
    List<DepositTransaction> findAllByUser(@Param("user") User user);

    @Query("select dt from DepositTransaction dt where dt.deposit.id=:depositId and dt.deposit.user=:user")
    List<DepositTransaction> findAllByDepositAndUser(@Param("depositId") Integer depositId, @Param("user") User user);
}
