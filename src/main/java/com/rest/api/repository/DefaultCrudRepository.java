package com.rest.api.repository;


import com.rest.api.model.config.AbstractModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;

import java.util.List;

@NoRepositoryBean
public interface DefaultCrudRepository<T extends AbstractModel> extends JpaRepository<T, Integer> {

    @Modifying
    @Query("UPDATE #{#entityName} x SET x.dataStatus = 0 WHERE x.id = :id")
    void delete(@Param("id") Integer id);


    @Query("SELECT x from #{#entityName} x WHERE x.id = :id")
    T findOne(@Param("id") Integer id);

    @Override
    List<T> findAll();

}