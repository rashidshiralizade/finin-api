package com.rest.api.repository;

import com.rest.api.model.User;

public interface UserRepository extends DefaultCrudRepository<User> {

    User findByUsername(String username);

}

