package com.rest.api.repository;

import com.rest.api.model.Deposit;
import com.rest.api.model.User;

import java.util.List;

public interface DepositRepository extends DefaultCrudRepository<Deposit> {
    List<Deposit> findAllByUser(User user);

    Deposit findFirstByUserAndId(User user,int id);
}
