package com.rest.api.repository;

import com.rest.api.model.DepositPeriod;

public interface DepositPeriodRepository extends DefaultCrudRepository<DepositPeriod> {

}
