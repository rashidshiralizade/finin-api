package com.rest.api.repository;

import com.rest.api.model.UserToken;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserTokenRepository extends DefaultCrudRepository<UserToken> {
    @Query("select ut from UserToken ut where ut.token=:token")
    UserToken findFirstByToken(@Param("token") String token);

}
