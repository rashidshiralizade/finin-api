package com.rest.api.security;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class DefaultAuthenticationProvider implements AuthenticationProvider {


    public DefaultAuthenticationProvider() {
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        DefaultAuthenticationToken defaultAuthenticationToken = (DefaultAuthenticationToken) authentication;
        return defaultAuthenticationToken;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return DefaultAuthenticationToken.class.isAssignableFrom(aClass);
    }
}
