package com.rest.api.security;

import com.rest.api.enums.ErrorEnum;
import com.rest.api.enums.UserStatusEnum;
import com.rest.api.exception.RestException;
import com.rest.api.model.User;
import com.rest.api.model.UserToken;
import com.rest.api.repository.UserRepository;
import com.rest.api.repository.UserTokenRepository;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DefaultAuthenticationFilter extends OncePerRequestFilter {

    private static String TOKEN_NAME = "Authorization";


    @Autowired
    private UserTokenRepository userTokenRepository;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        String tokenValue = request.getHeader(TOKEN_NAME);

        if (tokenValue != null && tokenValue.length() > 10) {

            if (tokenValue.startsWith("Bearer") || tokenValue.startsWith("bearer")) {
                tokenValue = tokenValue.substring(7, tokenValue.length());
            }

            String hashToken = DigestUtils.md5Hex(tokenValue);

            UserToken userToken = userTokenRepository.findFirstByToken(hashToken);

            if (userToken == null) {
                throw new RestException(ErrorEnum.TOKEN_NOT_FOUND);
            }

            User user = userToken.getUser();

            if (user == null) {
                request.getRequestDispatcher("/public/invalid/token").forward(request, response);
                return;
            } else if (user.getUserStatusEnum() == UserStatusEnum.PENDING && !request.getRequestURL().toString().contains("/private/user/business")) {
                request.getRequestDispatcher("/public/pending/user").forward(request, response);
            } else if (user.getUserStatusEnum() == UserStatusEnum.BLOCKED) {
                request.getRequestDispatcher("/public/blocked/user").forward(request, response);
            } else if (user != null) {
                Authentication auth = new DefaultAuthenticationToken(user);
                SecurityContextHolder.getContext().setAuthentication(auth);
                filterChain.doFilter(request, response);
                SecurityContextHolder.getContext().setAuthentication(null);
                return;
            }
        }
        filterChain.doFilter(request, response);
    }
}
