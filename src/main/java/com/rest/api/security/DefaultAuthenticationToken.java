package com.rest.api.security;


import com.rest.api.model.User;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Arrays;
import java.util.Collection;

public class DefaultAuthenticationToken extends AbstractAuthenticationToken {
    private User user;

    public DefaultAuthenticationToken(User user) {
        super(Arrays.asList());
        this.user = user;
    }

    public DefaultAuthenticationToken(Collection<? extends GrantedAuthority> authorities, Long uid) {
        super(authorities);
    }

    @Override
    public Object getCredentials() {
        return user.getId();
    }

    @Override
    public Object getPrincipal() {
        return user;
    }
}
