package com.rest.api.form;

import com.rest.api.constraint.Password;
import com.rest.api.constraint.Username;

import javax.validation.constraints.NotBlank;



public class RegisterForm {
    @NotBlank(message = "name_is_empty")
    private String name;
    @NotBlank(message = "surname_is_empty")
    private String surname;
    @Username
    private String username;
    @Password
    private String password;

    private String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
