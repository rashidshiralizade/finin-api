package com.rest.api.form;


public class DepositForm {

    private Integer id;

    private Integer idDepositPeriod;

    private Integer amount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdDepositPeriod() {
        return idDepositPeriod;
    }

    public void setIdDepositPeriod(Integer idDepositPeriod) {
        this.idDepositPeriod = idDepositPeriod;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

}
