package com.rest.api.form;

import com.rest.api.constraint.Fin;
import com.rest.api.constraint.SerialID;

import javax.validation.constraints.NotBlank;

public class UserForm {

    @NotBlank(message = "name_is_empty")
    private String name;
    @NotBlank(message = "surname_is_empty")
    private String surname;
    @SerialID
    private String serial;
    @Fin
    private String fin;

    private String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
