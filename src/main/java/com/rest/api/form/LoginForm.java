package com.rest.api.form;

import com.rest.api.constraint.Password;
import com.rest.api.constraint.Username;


public class LoginForm {

    @Username
    private String username;
    @Password
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
