package com.rest.api.model;

import com.rest.api.model.config.AbstractModel;
import com.rest.api.model.config.AbstractModel;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
@Author: Rashid Shiralizade
*/
@Entity
@Table(name = "deposit_period", catalog = "finin")
@DynamicInsert()
@DynamicUpdate()
@Where(clause = "data_status <> 0")
public class DepositPeriod extends AbstractModel implements java.io.Serializable {

    private int id;

    private Currency currency;

    private int period;

    private double percent;

    private List<Deposit> deposits = new ArrayList<Deposit>(0);

    public DepositPeriod() {
    }

    public DepositPeriod(int id, Currency currency, int period, double percent) {
        this.id = id;
        this.currency = currency;
        this.period = period;
        this.percent = percent;
    }

    public DepositPeriod(int id, Currency currency, int period, double percent, List<Deposit> deposits) {
        this.id = id;
        this.currency = currency;
        this.period = period;
        this.percent = percent;
        this.deposits = deposits;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id_deposit_period", unique = true, nullable = false)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_currency", nullable = false)
    @Where(clause = "data_status <> 0")
    public Currency getCurrency() {
        return this.currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Column(name = "period", nullable = false)
    public int getPeriod() {
        return this.period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    @Column(name = "percent", nullable = false, precision = 11, scale = 0)
    public double getPercent() {
        return this.percent;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "depositPeriod")
    @Where(clause = "data_status <> 0")
    public List<Deposit> getDeposits() {
        return this.deposits;
    }

    public void setDeposits(List<Deposit> deposits) {
        this.deposits = deposits;
    }
}
