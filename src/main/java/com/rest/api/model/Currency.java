package com.rest.api.model;

import com.rest.api.model.config.AbstractModel;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
@Author: Rashid Shiralizade
*/
@Entity
@Table(name = "currency", catalog = "finin")
@DynamicInsert()
@DynamicUpdate()
@Where(clause = "data_status <> 0")
public class Currency extends AbstractModel implements java.io.Serializable {

    private int id;

    private String name;

    private List<DepositPeriod> depositPeriods = new ArrayList<DepositPeriod>(0);

    public Currency() {
    }

    public Currency(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Currency(int id, String name, List<DepositPeriod> depositPeriods) {
        this.id = id;
        this.name = name;
        this.depositPeriods = depositPeriods;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id_currency", unique = true, nullable = false)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false, length = 5)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "currency")
    @Where(clause = "data_status <> 0")
    public List<DepositPeriod> getDepositPeriods() {
        return this.depositPeriods;
    }

    public void setDepositPeriods(List<DepositPeriod> depositPeriods) {
        this.depositPeriods = depositPeriods;
    }
}
