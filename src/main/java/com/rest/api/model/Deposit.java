package com.rest.api.model;


import com.rest.api.model.config.AbstractModel;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
@Author: Rashid Shiralizade
*/
@Entity
@Table(name = "deposit", catalog = "finin")
@DynamicInsert()
@DynamicUpdate()
@Where(clause = "data_status <> 0")
public class Deposit extends AbstractModel implements java.io.Serializable {

    private int id;

    private DepositPeriod depositPeriod;

    private User user;

    private int amount;

    private List<DepositTransaction> depositTransactions = new ArrayList<DepositTransaction>(0);

    public Deposit() {
    }

    public Deposit(int id, DepositPeriod depositPeriod, User user, int amount) {
        this.id = id;
        this.depositPeriod = depositPeriod;
        this.user = user;
        this.amount = amount;
    }

    public Deposit(int id, DepositPeriod depositPeriod, User user, int amount, List<DepositTransaction> depositTransactions) {
        this.id = id;
        this.depositPeriod = depositPeriod;
        this.user = user;
        this.amount = amount;
        this.depositTransactions = depositTransactions;
    }

    @Id     @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id_deposit", unique = true, nullable = false)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_deposit_period", nullable = false)
    @Where(clause = "data_status <> 0")
    public DepositPeriod getDepositPeriod() {
        return this.depositPeriod;
    }

    public void setDepositPeriod(DepositPeriod depositPeriod) {
        this.depositPeriod = depositPeriod;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user", nullable = false)
    @Where(clause = "data_status <> 0")
    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = "amount", nullable = false)
    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "deposit")
    @Where(clause = "data_status <> 0")
    public List<DepositTransaction> getDepositTransactions() {
        return this.depositTransactions;
    }

    public void setDepositTransactions(List<DepositTransaction> depositTransactions) {
        this.depositTransactions = depositTransactions;
    }
}
