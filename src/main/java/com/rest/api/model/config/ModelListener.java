package com.rest.api.model.config;

import com.rest.api.enums.DataStatusEnum;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.util.Date;

public class ModelListener {

    @PrePersist
    public void onPrePersist(AbstractModel model) {
        model.setDataStatusEnum(DataStatusEnum.ACTIVE);
        model.setInsertDate(new Date());
    }

    @PreUpdate
    public void onPreUpdate(AbstractModel model) {
        model.setDataStatusEnum(DataStatusEnum.UPDATED);
        model.setUpdateDate(new Date());
    }
}
