package com.rest.api.model.config;

import com.rest.api.enums.DataStatusEnum;

import javax.persistence.*;
import java.util.Date;

@EntityListeners(ModelListener.class)
@MappedSuperclass
public abstract class AbstractModel {

    private Date insertDate;

    private Date updateDate;

    private Short dataStatus;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "insert_date", length = 19)
    public Date getInsertDate() {
        return this.insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_date", length = 19)
    public Date getUpdateDate() {
        return this.updateDate;
    }

    protected void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Column(name = "data_status")
    public Short getDataStatus() {
        return this.dataStatus;
    }

    protected void setDataStatus(Short dataStatus) {
        this.dataStatus = dataStatus;
    }

    @Transient
    public DataStatusEnum getDataStatusEnum() {
        return DataStatusEnum.getDataStatus(dataStatus);
    }

    protected void setDataStatusEnum(DataStatusEnum dataStatus) {
        this.dataStatus = dataStatus.getValue();
    }
}