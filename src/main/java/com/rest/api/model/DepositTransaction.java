package com.rest.api.model;

import com.rest.api.enums.TransactionTypeEnum;
import com.rest.api.model.config.AbstractModel;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
@Author: Rashid Shiralizade
*/
@Entity
@Table(name = "deposit_transaction", catalog = "finin")
@DynamicInsert()
@DynamicUpdate()
@Where(clause = "data_status <> 0")
public class DepositTransaction extends AbstractModel implements java.io.Serializable {

    private int id;

    private Deposit deposit;

    private int amount;

    private Short transactionType;

    public DepositTransaction() {
    }

    public DepositTransaction(int id, Deposit deposit, int amount, Short transactionType) {
        this.id = id;
        this.deposit = deposit;
        this.amount = amount;
        this.transactionType = transactionType;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id_deposit_transaction", unique = true, nullable = false)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_deposit", nullable = false)
    @Where(clause = "data_status <> 0")
    public Deposit getDeposit() {
        return this.deposit;
    }

    public void setDeposit(Deposit deposit) {
        this.deposit = deposit;
    }

    @Column(name = "amount", nullable = false)
    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Column(name = "transaction_type", nullable = false)
    public Short getTransactionType() {
        return this.transactionType;
    }

    public void setTransactionType(Short transactionType) {
        this.transactionType = transactionType;
    }

    @Transient()
    public TransactionTypeEnum getTransactionTypeEnum() {
        return TransactionTypeEnum.getTransactionType(this.transactionType);
    }

    public void setTransactionTypeEnum(TransactionTypeEnum transactionType) {
        this.transactionType = transactionType.getValue();
    }
}
