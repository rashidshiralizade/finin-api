package com.rest.api.model;

import com.rest.api.model.config.AbstractModel;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @Author: Rashid Shiralizade
 */
@Entity
@Table(name = "user_token", catalog = "finin")
@DynamicInsert()
@DynamicUpdate()
@Where(clause = "data_status <> 0")
public class UserToken extends AbstractModel implements java.io.Serializable {

    private int id;

    private User user;

    private String token;

    public UserToken() {
    }

    public UserToken(int id, User user, String token) {
        this.id = id;
        this.user = user;
        this.token = token;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id_user_token", unique = true, nullable = false)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user", nullable = false)
    @Where(clause = "data_status <> 0")
    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = "token", nullable = false)
    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
