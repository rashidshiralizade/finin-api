package com.rest.api.model;

import com.rest.api.enums.UserStatusEnum;
import com.rest.api.model.config.AbstractModel;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * @Author: Rashid Shiralizade
 */

@Entity
@Table(name = "user", catalog = "finin")
@DynamicInsert()
@DynamicUpdate()
@Where(clause = "data_status <> 0")
public class User extends AbstractModel implements java.io.Serializable {

    private int id;

    private String name;

    private String surname;

    private String username;

    private String password;

    private String salt;

    private String role;

    private Short userStatus;

    private String phone;

    private String fin;

    private String serial;

    private List<Deposit> deposits = new ArrayList<Deposit>(0);

    public User() {
    }

    public User(int id, String name, String surname, String username) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.username = username;
    }

    public User(int id, String name, String surname, String username, String password, String salt, String role, Short userStatus, String phone, List<Deposit> deposits) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.salt = salt;
        this.role = role;
        this.userStatus = userStatus;
        this.phone = phone;
        this.deposits = deposits;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id_user", unique = true, nullable = false)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "surname", nullable = false, length = 45)
    public String getSurname() {
        return this.surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Column(name = "username", nullable = false, length = 50)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "password", length = 500)
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "salt", length = 500)
    public String getSalt() {
        return this.salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @Column(name = "role", length = 45)
    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Column(name = "user_status")
    public Short getUserStatus() {
        return this.userStatus;
    }

    public void setUserStatus(Short userStatus) {
        this.userStatus = userStatus;
    }

    @Column(name = "phone", length = 35)
    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    @Where(clause = "data_status <> 0")
    public List<Deposit> getDeposits() {
        return this.deposits;
    }

    public void setDeposits(List<Deposit> deposits) {
        this.deposits = deposits;
    }

    @Column(name = "fin", length = 7)
    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    @Column(name = "serial", length = 8)
    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    @Transient()
    public UserStatusEnum getUserStatusEnum() {
        return UserStatusEnum.getUserStatus(this.userStatus);
    }

    public void setUserStatusEnum(UserStatusEnum userStatus) {
        this.userStatus = userStatus.getValue();
    }
}
