package com.rest.api.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.lang.reflect.Field;

@Component
public class FileUploadFolder {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadFolder.class);


    public String FOLDER_BUSINESS_LOGO = "businesslogo";
    public String FOLDER_PRODUCT_IMAGE = "productimage";


    @Autowired
    public FileUploadFolder(@Value("${root_path}") String rootPath) throws Exception {
        Field[] fields = this.getClass().getFields();


        LOGGER.info("Upload folder init");
        for (Field field : fields) {
            if (!field.getName().startsWith("FOLDER")) {
                continue;
            }


            File file = new File(rootPath + File.separator + field.get(this));
            field.set(this, file.getAbsolutePath() + File.separator);


            file.mkdirs();


            LOGGER.info(field.getName() + ": " + field.get(this));
        }
    }
}
