package com.rest.api.controller;

import com.rest.api.enums.ErrorEnum;
import com.rest.api.response.GeneralResponse;
import com.rest.api.service.DepositPeriodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/private/depositPeriod")
@Validated
public class DepositPeriodController {

    private DepositPeriodService depositPeriodService;

    @Autowired
    public DepositPeriodController(DepositPeriodService depositPeriodService) {
        this.depositPeriodService = depositPeriodService;
    }

    @GetMapping
    public GeneralResponse all() {
        return new GeneralResponse(ErrorEnum.OK, depositPeriodService.all());
    }

}
