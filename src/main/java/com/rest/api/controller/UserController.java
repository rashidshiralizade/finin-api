package com.rest.api.controller;

import com.rest.api.enums.ErrorEnum;
import com.rest.api.form.UserForm;
import com.rest.api.model.User;
import com.rest.api.response.GeneralResponse;
import com.rest.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/private/user")
@Validated
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PutMapping
    public GeneralResponse update(@Valid @RequestBody UserForm form, User user) {
        return new GeneralResponse(ErrorEnum.OK, userService.update(user, form));
    }

    @GetMapping
    public GeneralResponse info(User user) {
        return new GeneralResponse(ErrorEnum.OK, userService.info(user));
    }

}
