package com.rest.api.controller;

import com.rest.api.enums.ErrorEnum;
import com.rest.api.response.GeneralResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/public")
public class SecurityController {


    @ResponseStatus(HttpStatus.FORBIDDEN)
    @RequestMapping(value = "/access/denied", method = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.PUT, RequestMethod.POST})
    public GeneralResponse accessDenied() {
        return new GeneralResponse(ErrorEnum.ACCESS_DENIED);
    }

    @RequestMapping(value = "/blocked/user", method = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.PUT, RequestMethod.POST})
    public GeneralResponse blockedUser() {
        return new GeneralResponse(ErrorEnum.USER_BLOCKED);
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @RequestMapping(value = "/invalid/token", method = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.PUT, RequestMethod.POST})
    public GeneralResponse invalidToken() {
        return new GeneralResponse(ErrorEnum.INVALID_TOKEN);
    }

    @RequestMapping(value = "/pending/user", method = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.PUT, RequestMethod.POST})
    public GeneralResponse pendingUser() {
        return new GeneralResponse(ErrorEnum.USER_PENDING);
    }
}
