package com.rest.api.controller;

import com.rest.api.enums.ErrorEnum;
import com.rest.api.form.DepositForm;
import com.rest.api.model.User;
import com.rest.api.response.GeneralResponse;
import com.rest.api.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/private/deposit")
@Validated
public class DepositController {

    private DepositService depositService;

    @Autowired
    public DepositController(DepositService depositService) {
        this.depositService = depositService;
    }


    @GetMapping
    public GeneralResponse all(User user) {
        return new GeneralResponse(ErrorEnum.OK, depositService.all(user));
    }

    @PostMapping
    public GeneralResponse add(@Valid @RequestBody DepositForm form, User user) throws Exception {
        depositService.add(form, user);
        return new GeneralResponse(ErrorEnum.OK);
    }

    @GetMapping("/{id}")
    public GeneralResponse getTransactions(User user, @PathVariable Integer id) {
        return new GeneralResponse(ErrorEnum.OK, depositService.getTransactionByDeposit(id, user));
    }

}
