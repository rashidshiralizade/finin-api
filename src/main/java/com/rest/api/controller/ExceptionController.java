package com.rest.api.controller;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.rest.api.enums.ErrorEnum;
import com.rest.api.exception.RestException;
import com.rest.api.response.GeneralResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.ConstraintViolationException;
import java.util.Locale;

@RestController
@ControllerAdvice
public class ExceptionController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionController.class);

    @Autowired
    private MessageSource messageSource;

    private static final String ERROR = "error.";

    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(RestException.class)
    public GeneralResponse handler(RestException ex, Locale locale) {
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Error code : " + ex.getCode() + " Error message : " + ex.getMessage());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("ERROR : " + ex);
        }
        return new GeneralResponse(ex.getCode(), messageSource.getMessage(ERROR + ex.getMessage(), null, locale));
    }

    @ResponseStatus(value = HttpStatus.OK)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public GeneralResponse handleException(MethodArgumentNotValidException ex, Locale locale) {

        FieldError fieldError = ex.getBindingResult().getFieldError();
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Error  message : " + fieldError.getDefaultMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("ERROR : " + ex);
        }
        return new GeneralResponse(ErrorEnum.VALIDATION_ERROR.getValue(), messageSource.getMessage(ERROR + ErrorEnum.VALIDATION_ERROR.name().toLowerCase(), null, locale));
    }

    @ResponseStatus(value = HttpStatus.OK)
    @ExceptionHandler(BindException.class)
    public GeneralResponse handleException(BindException ex, Locale locale) {
        FieldError fieldError = ex.getFieldError();
        String message = fieldError.getDefaultMessage();
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Error  message : " + message);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("ERROR : " + ex);
        }
        return new GeneralResponse(ErrorEnum.VALIDATION_ERROR.getValue(), messageSource.getMessage(ERROR + ErrorEnum.VALIDATION_ERROR.name().toLowerCase(), null, locale));

    }

    @ResponseStatus(value = HttpStatus.OK)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public GeneralResponse handleException(HttpMessageNotReadableException ex, Locale locale) {

        String message = null;
        if (ex.getCause() instanceof InvalidFormatException) {
            InvalidFormatException invalidFormatException = (InvalidFormatException) ex.getCause();
            String fieldName = invalidFormatException.getPath().get(0).getFieldName();
            Class<?> type = invalidFormatException.getTargetType();

            message = String.format("wrong value for %s type parameter %s", type, fieldName);
        } else if (ex.getCause() instanceof JsonMappingException) {
            JsonMappingException jsonMappingException = (JsonMappingException) ex.getCause();

            message = String.format("wrong value for int parameter %s",
                    jsonMappingException.getPath().get(0).getFieldName());
        } else {
            message = ex.getMessage();
        }
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Error  message : " + message);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("ERROR : " + ex);
        }

        return new GeneralResponse(ErrorEnum.VALIDATION_ERROR.getValue(), messageSource.getMessage(ERROR + ErrorEnum.VALIDATION_ERROR.name().toLowerCase(), null, locale));

    }

    @ResponseStatus(value = HttpStatus.OK)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public GeneralResponse handleNotFoundError(HttpRequestMethodNotSupportedException ex) {

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Error  message : " + ex.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("ERROR : " + ex);
        }
        return new GeneralResponse(ErrorEnum.INVALID_URL);
    }


    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler({ConstraintViolationException.class})
    public Object handleException(ConstraintViolationException violationException, Locale locale) {
        String msg = violationException.getConstraintViolations().iterator().next().getMessage();

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Error message :" + msg);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("ERROR", violationException);
        }
        return new GeneralResponse(ErrorEnum.VALIDATION_ERROR.getValue(), messageSource.getMessage(ERROR + ErrorEnum.VALIDATION_ERROR.name().toLowerCase(), null, locale));

    }

    @ResponseStatus(value = HttpStatus.OK)
    @ExceptionHandler(NoHandlerFoundException.class)
    public GeneralResponse handleNotFoundError(NoHandlerFoundException ex) {

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Error  message : " + ex.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("ERROR : " + ex);
        }
        return new GeneralResponse(ErrorEnum.INVALID_URL);
    }


    @ResponseStatus(value = HttpStatus.OK)
    @ExceptionHandler(Throwable.class)
    public GeneralResponse handleException(Throwable ex) {

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Error  message : " + ex.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("ERROR : " + ex);
        }
        return new GeneralResponse(ErrorEnum.SERVER_ERROR);
    }

}
