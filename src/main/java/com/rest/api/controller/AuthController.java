package com.rest.api.controller;


import com.rest.api.constraint.Username;
import com.rest.api.enums.ErrorEnum;
import com.rest.api.form.LoginForm;
import com.rest.api.form.RegisterForm;
import com.rest.api.response.GeneralResponse;
import com.rest.api.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;


@RestController
@RequestMapping("/public/auth")
@Validated
public class AuthController {

    private AuthService authService;

    private MessageSource messageSource;

    @Autowired
    public AuthController(AuthService authService, MessageSource messageSource) {
        this.authService = authService;
        this.messageSource = messageSource;
    }

    @PostMapping("/login")
    public GeneralResponse login(@Valid @RequestBody LoginForm form) throws Exception {
        return new GeneralResponse(ErrorEnum.OK, authService.login(form));
    }

    @PostMapping("/register")
    public GeneralResponse register(@Valid @RequestBody RegisterForm form) throws Exception {
        return new GeneralResponse(ErrorEnum.OK, authService.register(form));
    }

}
