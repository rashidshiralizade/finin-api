package com.rest.api.service;

import com.rest.api.model.DepositPeriod;

import com.rest.api.repository.DepositPeriodRepository;
import com.rest.api.response.DepositPeriodResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DepositPeriodService {

    private DepositPeriodRepository depositPeriodRepository;

    @Autowired
    public DepositPeriodService(DepositPeriodRepository depositPeriodRepository) {
        this.depositPeriodRepository = depositPeriodRepository;
    }

    public List<DepositPeriodResponse> all() {
        List<DepositPeriodResponse> list = new ArrayList<>();
        List<DepositPeriod> depositPeriods = depositPeriodRepository.findAll();
        for (DepositPeriod deposit : depositPeriods) {
            DepositPeriodResponse depositPeriodResponse = new DepositPeriodResponse();
            depositPeriodResponse.setId(deposit.getId());
            depositPeriodResponse.setCurrency(deposit.getCurrency().getName());
            depositPeriodResponse.setPercent(deposit.getPercent());
            depositPeriodResponse.setPeriod(deposit.getPeriod());
            list.add(depositPeriodResponse);
        }
        return list;
    }

}
