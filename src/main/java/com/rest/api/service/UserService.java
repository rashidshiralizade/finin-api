package com.rest.api.service;

import com.rest.api.form.UserForm;
import com.rest.api.model.User;
import com.rest.api.repository.UserRepository;
import com.rest.api.response.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    public UserResponse update(User user, UserForm userForm) {
        user.setPhone(userForm.getPhone());
        user.setName(userForm.getName());
        user.setSurname(userForm.getSurname());
        user.setFin(userForm.getFin());
        user.setSerial(userForm.getSerial());
        user = userRepository.save(user);

        UserResponse userResponse = new UserResponse();
        userResponse.setEmail(user.getUsername());
        userResponse.setFin(user.getFin());
        userResponse.setName(user.getName());
        userResponse.setSurname(user.getSurname());
        userResponse.setSerial(user.getSerial());
        userResponse.setPhone(user.getPhone());

        return userResponse;
    }

    public UserResponse info(User user) {
        UserResponse userResponse = new UserResponse();
        userResponse.setEmail(user.getUsername());
        userResponse.setFin(user.getFin());
        userResponse.setName(user.getName());
        userResponse.setSurname(user.getSurname());
        userResponse.setSerial(user.getSerial());
        userResponse.setPhone(user.getPhone());
        return userResponse;
    }

}
