package com.rest.api.service;

import com.rest.api.builder.UserBuilder;
import com.rest.api.cyrpto.UserPasswordEncoder;
import com.rest.api.enums.*;
import com.rest.api.exception.RestException;
import com.rest.api.form.LoginForm;
import com.rest.api.form.RegisterForm;
import com.rest.api.model.*;
import com.rest.api.repository.*;
import com.rest.api.response.LoginResponse;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class AuthService {

    private UserRepository userRepository;


    private UserTokenRepository userTokenRepository;

    @Autowired
    public AuthService(UserRepository userRepository, UserTokenRepository userTokenRepository) {
        this.userRepository = userRepository;
        this.userTokenRepository = userTokenRepository;
    }

    @Transactional
    public LoginResponse login(LoginForm form) throws Exception {
        User user = userRepository.findByUsername(form.getUsername());
        if (user == null) {
            throw new RestException(ErrorEnum.USERNAME_OR_PASSWORD_WRONG);
        }

        if (user.getUserStatusEnum() == UserStatusEnum.BLOCKED) {
            throw new RestException(ErrorEnum.USER_BLOCKED);
        }

        UserPasswordEncoder userPasswordEncoder = UserPasswordEncoder.create(user.getPassword(), user.getSalt());
        if (!userPasswordEncoder.compare(form.getPassword())) {
            throw new RestException(ErrorEnum.USERNAME_OR_PASSWORD_WRONG);
        }
        String token = UUID.randomUUID().toString();
        String hashToken = DigestUtils.md5Hex(token);
        UserToken userToken = new UserToken();
        userToken.setUser(user);
        userToken.setToken(hashToken);
        userTokenRepository.save(userToken);
        boolean status = true;
        return new LoginResponse(token, status);
    }

    @Transactional
    public LoginResponse register(RegisterForm form) {
        User user = userRepository.findByUsername(form.getUsername());
        if (user != null) {
            throw new RestException(ErrorEnum.USERNAME_ALREADY_EXISTS);
        }
        user = UserBuilder.user()//
                .username(form.getUsername())//
                .password(form.getPassword())//
                .name(form.getName())//
                .role("USER")
                .surname(form.getSurname())//
                .phone(form.getPhone())//
                .userStatus(UserStatusEnum.COMPLETED)//
                .build();

        user = userRepository.save(user);
        String token = UUID.randomUUID().toString();
        String hashToken = DigestUtils.md5Hex(token);

        UserToken userToken = new UserToken();
        userToken.setUser(user);
        userToken.setToken(hashToken);
        userTokenRepository.save(userToken);
        return new LoginResponse(token, false);
    }

}
