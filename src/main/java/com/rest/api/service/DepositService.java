package com.rest.api.service;

import com.rest.api.enums.ErrorEnum;
import com.rest.api.enums.TransactionTypeEnum;
import com.rest.api.exception.RestException;
import com.rest.api.form.DepositForm;
import com.rest.api.model.Deposit;
import com.rest.api.model.DepositPeriod;
import com.rest.api.model.DepositTransaction;
import com.rest.api.model.User;
import com.rest.api.repository.DepositPeriodRepository;
import com.rest.api.repository.DepositRepository;
import com.rest.api.repository.DepositTransactionRepository;
import com.rest.api.response.DepositPeriodResponse;
import com.rest.api.response.DepositResponse;
import com.rest.api.response.DepositTransactionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class DepositService {

    private DepositRepository depositRepository;

    private DepositPeriodRepository depositPeriodRepository;

    private DepositTransactionRepository depositTransactionRepository;

    @Autowired
    public DepositService(DepositRepository depositRepository, DepositPeriodRepository depositPeriodRepository, DepositTransactionRepository depositTransactionRepository) {
        this.depositRepository = depositRepository;
        this.depositPeriodRepository = depositPeriodRepository;
        this.depositTransactionRepository = depositTransactionRepository;
    }


    public List<DepositResponse> all(User user) {
        List<DepositResponse> list = new ArrayList<>();
        List<Deposit> depositList = depositRepository.findAllByUser(user);
        List<DepositTransactionResponse> depositTransactionResponses = new ArrayList<>();
        for (Deposit deposit : depositList) {
            DepositResponse depositResponse = new DepositResponse();
            depositResponse.setAmount(deposit.getAmount());
            depositResponse.setId(deposit.getId());
            depositResponse.setIdUser(deposit.getUser().getId());
            depositResponse.setInsertDate(deposit.getInsertDate());
            DepositPeriod depositPeriod = depositPeriodRepository.findOne(deposit.getDepositPeriod().getId());
            DepositPeriodResponse depositPeriodResponse = new DepositPeriodResponse();
            depositPeriodResponse.setId(depositPeriod.getId());
            depositPeriodResponse.setCurrency(depositPeriod.getCurrency().getName());
            depositPeriodResponse.setPercent(depositPeriod.getPercent());
            depositPeriodResponse.setPeriod(depositPeriod.getPeriod());
            depositResponse.setDepositPeriodResponse(depositPeriodResponse);

            List<DepositTransaction> depositTransactionList = depositTransactionRepository.findAllByDeposit(deposit);
            if (depositTransactionList.size() != 0) {
                for (DepositTransaction dt : depositTransactionList) {
                    DepositTransactionResponse dr = new DepositTransactionResponse();
                    dr.setId(dt.getId());
                    dr.setAmount(dt.getAmount());
                    dr.setTransactionType(dt.getTransactionType());
                    depositTransactionResponses.add(dr);
                }
            }
            depositResponse.setDepositTransactionResponse(depositTransactionResponses);
            list.add(depositResponse);
        }
        return list;
    }

    @Transactional
    public void add(DepositForm form, User user) {

        if (user.getFin() == null || user.getSerial() == null) {
            throw new RestException(ErrorEnum.DENIED_ADD_DEPOSIT);
        }

        DepositPeriod depositPeriod = depositPeriodRepository.findOne(form.getIdDepositPeriod());

        if (depositPeriod == null) {
            throw new RestException(ErrorEnum.DEPOSIT_PERIOD_NOT_FOUND);
        }

        Deposit deposit = new Deposit();
        TransactionTypeEnum transactionTypeEnum = TransactionTypeEnum.getTransactionType(TransactionTypeEnum.ADD.getValue());
        if (form.getId() != null) {
            deposit = depositRepository.findFirstByUserAndId(user, form.getId());
            if (deposit == null) {
                throw new RestException(ErrorEnum.DEPOSIT_NOT_FOUND);
            }
            deposit.setAmount(form.getAmount() + deposit.getAmount());
        } else {
            deposit.setAmount(form.getAmount());
        }
        deposit.setDepositPeriod(depositPeriod);
        deposit.setUser(user);
        deposit = depositRepository.save(deposit);


        DepositTransaction depositTransaction = new DepositTransaction();
        depositTransaction.setAmount(form.getAmount());
        depositTransaction.setDeposit(deposit);
        depositTransaction.setTransactionType(transactionTypeEnum.getValue());
        depositTransactionRepository.save(depositTransaction);
    }

    public List<DepositTransactionResponse> getTransactionByDeposit(Integer id, User user) {
        List<DepositTransactionResponse> transactionResponseList = new ArrayList<>();
        List<DepositTransaction> depositTransactions = new ArrayList<>();
        if (id.equals(0)) {
            depositTransactions = depositTransactionRepository.findAllByUser(user);
        } else {
            depositTransactions = depositTransactionRepository.findAllByDepositAndUser(id, user);
        }
        for (DepositTransaction depositTransaction : depositTransactions) {
            DepositTransactionResponse depositTransactionResponse = new DepositTransactionResponse();
            depositTransactionResponse.setId(depositTransaction.getId());
            depositTransactionResponse.setAmount(depositTransaction.getAmount());
            depositTransactionResponse.setTransactionType(depositTransaction.getTransactionTypeEnum().getValue());
            depositTransactionResponse.setIdDeposit(depositTransaction.getDeposit().getId());
            depositTransactionResponse.setCurrency(depositTransaction.getDeposit().getDepositPeriod().getCurrency().getName());
            transactionResponseList.add(depositTransactionResponse);
        }
        return transactionResponseList;
    }

}
