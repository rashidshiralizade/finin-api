package com.rest.api.builder;

import com.rest.api.cyrpto.UserPasswordEncoder;

import com.rest.api.enums.UserStatusEnum;
import com.rest.api.model.User;


/**
 * Builder pattern - çoxlu sayda parametr qəbul edən metodlar üçün
 * builder patterndən istifadə edirəm. Həmçinin metodda məcburiyyət olmayan sütunlar üçün
 * istifadə edilməyi də məqsədəuyundur.
 * User qeydiyyatı zamanı bundan istifadə etmişəm.
 */
public class UserBuilder {
    private String name;
    private String surname;
    private String username;
    private String password;
    private String role;
    private String phone;
    private UserStatusEnum userStatus;


    public static UserBuilder user() {
        return new UserBuilder();
    }

    public UserBuilder name(String name) {
        this.name = name;
        return this;
    }

    public UserBuilder surname(String surname) {
        this.surname = surname;
        return this;
    }

    public UserBuilder username(String username) {
        this.username = username;
        return this;
    }

    public UserBuilder password(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder role(String role) {
        this.role = role;
        return this;
    }

    public UserBuilder phone(String phone) {
        this.phone = phone;
        return this;
    }

    public UserBuilder userStatus(UserStatusEnum userStatus) {
        this.userStatus = userStatus;
        return this;
    }

    public User build() {
        User user = new User();
        if (password != null) {
            UserPasswordEncoder userPasswordEncoder = UserPasswordEncoder.create(password);
            user.setPassword(userPasswordEncoder.getHash());
            user.setSalt(userPasswordEncoder.getSalt());
        }
        user.setName(name);
        user.setSurname(surname);
        user.setUserStatusEnum(userStatus);
        user.setUsername(username);
        user.setPhone(phone);
        user.setRole(role);
        return user;
    }
}
