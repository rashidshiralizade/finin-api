package com.rest.api.constraint;


import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@NotBlank(message = "password_is_empty")
@Size(min = 6, max = 25, message = "password_is_long_or_short")
public @interface Password {
    String message() default "password_is_not_valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
