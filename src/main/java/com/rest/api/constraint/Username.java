package com.rest.api.constraint;

import com.rest.api.constant.RegexConstant;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@NotBlank(message = "username_is_empty")
@Pattern(regexp = RegexConstant.EMAIL, message = "username_is_not_valid")
@Size(max = 45, message = "username_is_long")
public @interface Username {
    String message() default "username_is_not_valid";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
