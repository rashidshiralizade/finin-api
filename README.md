#FinIn API

##İstifadə edilən texnologiyalar:

1. Spring Boot
2. Spring Security
3. Swagger UI
4. Hibernate/JPA
5. Git
6. MySql
7. Builder Pattern


- ![#1589F0](https://spring.io/img/favicon-ca31b78daf0dd9a106bbf3c6d87d4ec7.png) SWAGGER URL : http://dxxxxxxxxxx:8081/finin/swagger-ui.html - ![#1589F0](https://spring.io/img/favicon-ca31b78daf0dd9a106bbf3c6d87d4ec7.png)

- ![#c5f015](https://spring.io/img/favicon-ca31b78daf0dd9a106bbf3c6d87d4ec7.png) API URL : http://xxxxxxxxxx.63:8081/finin - ![#c5f015](https://spring.io/img/favicon-ca31b78daf0dd9a106bbf3c6d87d4ec7.png)

#Finİn app

Bu tətbiq banka getmədən insanlara online şəkildə depozit hesabı yaratmaq imkanı verir

Daha ətrafli təqdimatda danışılacaq.



##Funsionallıqlar

Qeydiyyatdan keçmək və daxil olmaq funksiyası
Onlayn şəkildə kartla depozit yatırmaq funksiyası
Bir istifadəçi üçün bir neçə depozit yaratmaq imkanı
Depozit hesabların qrafiklərlə əks olunması
Hər bir depozit hesaba aid tranzaksiyaların tarixçəsi
Depozitin ödənişi zamanı sürətli kart oxuma funksiyası
Depozitdən gəlirin hesablanması üçün xüsusi kalkulyator imkanı
İstifadəçinin hesab məlumatlarının göstərilməsi və idarə olunması imkanı.
Popup menu vasitəsilə tətbiqdən çıxış imkanı.




###İşləmə prinsipləri

Qeydiyyatdan keçmək və daxil olmaq funksiyası
	

	Qeydiyyatdan keçmək üçün istifadəçidən name, surname,  email,  phone(optional), password melumatları tələb olnur. Xanalara Validation - lar tətbiq  			olmuşdur.

	Daxil ol zamanı istifadəçi email və passwordu daxil etməlidir. Xanalara Validation - lar tətbiq olmuşdur.



Onlayn şəkildə kartla depozit yatırmaq funksiyası


	Bu səhifə popup menu da 2ci düymənin click-i zamanı açılır. Daxil olaraq verilmiş seçimləri doldurmaq lazımdır. İlk öncə Seçimlərə əsasən istifadəçiyə aylıq 		geliri, seçdiyi müddət ərzindəki cəmi gəliri və depozitlə birgə son məbləği göstərir. Daha sonra istifadəçi nəticə ilə razılaşarsa ekranın yuxarı sağ küncündəki 		NEXT düyməsinə click etməlidir. Növbəti açılan səhifədə scan üçün düymə yerləşdirilmişdir. Depoziti yerləşdirmək üçün lazım olan kartı scan edərək kart 		üzərindəki məlumatlar avtomatik xanalara doldurulur. Daha sonra bu səhifədə Pay düyməsinə click etməklə uğurlu və ya uğursuz səhifəsi 	açılacaqdır. Bu 		səhifədən popup menu vasitəsilə digər 	səhifələrə keçmək olar. Qeyd edim ki, istifadəçi Account sehifəsində fin code ve serial number melumatlarını daxil 		etməyibsə depozit yerləşdirə bilməz.



Bir istifadəçi üçün bir neçə depozit yaratmaq imkan 


	2 - ci funksionallığı təkrar tətbiq etməklə depozit sayını artırmaq mümkündür.



Depozit hesabların qrafiklərlə əks olunması


       	Mobil tətbiqə daxil olarkən ilk olaraq açılan səhifədə qrafikləri görmək olar. Popup menu da isə HOME düyməsinə click etməklə keçmək olar. Əgər istifadəçi 		depozit yatırmayıbsa heç bir qrafik əks olunmur. 



Hər bir depozit hesaba aid tranzaksiyaların tarixçəsi


	Bu sehifəyə  popup menudan Tranzaksiya düyməsinə click etməklə keçid etmək olar. Açılan səhifədə istifadəçiyə aid olan tranzaksiyaların depozit hesabları 		üzrə qruplaşması göstərilmişdir.  Eyni zamanda depozitlərin siyahısından hansısa bir depozitə click etməklə həmin depozitə aid olan tranzaksiyalarln siyahısını 	görmək olar.



Depozitin ödənişi zamanı sürətli kart oxuma funksiyası


	2-ci funksionallığın tərkibində qeyd olunmuşdur.



Depozitdən gəlirin hesablanması üçün xüsusi kalkulyator imkanı


	Bu səhifəyə popup menudan Calc düyməsinə click etməklə keçmək olar.  Burada istifadəçi müxtəlif seçimlər etməklə depozitdən gelirini asanlıqla hesablaya 		bilər.



İstifadəçinin hesab məlumatlarının göstərilməsi və idarə olunması imkanı


	Bu sehifəyə popup menu dan Account düyməsinə click etməklə keçmək olar. Burada istifadəçi name, surname, phone, fin code, serial number melumatlarını 		mütləq daxil etmelidir. 






